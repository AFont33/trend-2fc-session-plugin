# !/usr/bin/python3
# -*- coding: utf-8 -*-

""" session_window.py

"""

import logging

from pyforms import conf

import numpy as np

from AnyQt.QtWidgets import QMessageBox

from AnyQt.QtGui import QColor, QBrush
from AnyQt.QtCore import QTimer, QEventLoop, QAbstractTableModel, Qt, QSize, QVariant, pyqtSignal

from pybpodapi.session import Session
from pyforms import BaseWidget
from pyforms.controls import ControlProgress
from pyforms.controls import ControlText
from pyforms.controls import ControlMatplotlib
from pyforms.controls import ControlLabel
from pyforms.controls import ControlButton

from trend_2fc_session_plugin.myData import strToData

import logging

import os

logger = logging.getLogger(__name__)


class SessionTrend(BaseWidget):
    """ Plugin main window """

    COUNT = 0 # Counter to rebember the index of data of the query.

    def __init__(self, session):
        BaseWidget.__init__(self, session.name)

        self.layout().setContentsMargins(5, 5, 5, 5) # Set graphic margin
        self.session = session # Session Class of the pybpod to get data

        # Form creatiion
        self._graph = ControlMatplotlib('Graph')
        self._updatebutton = ControlButton('Update')
        self._saveButton = ControlButton('Save')
        self._valGraph = ControlText('Values to show. [-1 for all]: ', default='5', helptext='Values that are plotting in the graph.')
        self._titleTrend = ControlLabel('Trend of 2FC task.')
        self._progress = ControlProgress('Loading', 0, 1, 100)

        # Form position
        self._formset = [
            ('_titleTrend', '_valGraph','_updatebutton', '_saveButton'),
            '_graph',
            '_progress'
        ]

        # Assign an action to the button
        self._saveButton.value = self.__saveAction

        # Assign an action to the button
        self._updatebutton.value = self.__updateAction

        # Class containing all the data     
        self._dt = strToData()

        # Set Property 
        self._progress.hide() # hide the progession bar
        self._graph.value = self.__on_draw # define the action to the graph.

        # Set the timer to recall the function update
        self._timer = QTimer()
        self._timer.timeout.connect(self.update)
        
        # Vector to contain the choices vector that will be extract from the data
        self.variaiables = None  

        print(self.session.data.head())

        m = self.session.data.query("TYPE=='VAL' and MSG=='EXPERIMENT_TYPE'")
        for p,o in m.iterrows():
            #print(o['+INFO'])
            #print(type(o['+INFO']))
            self._dt.define_experiment_type(str(o['+INFO']))

    def __saveAction(self):
        """ Save button: packs the relevant data into a text file for later analysis"""
        try:
            with open("savetest.txt", 'w') as f:
                #print(os.getcwd())
                #print("1:" + str(self._dt._experiment_type))
                f.write(str(self._dt._data) + ";" + self._dt._experiment_type)
        except Exception:
            self.warning("Something went wrong when saving the file")


    def __updateAction(self):
        """ Action Button """
        # Check the value and update the graph
        try:
            v = int(self._valGraph.value) # transform the string value to int
            if v > 0 or v == -1: # it's ok only in this case
                self._dt.MAXVAL_TOSHOW = v
                self._graph.draw()
            else: # otherwise show poupup message and reset the value
                self.warning('Only integer grater than zero or -1 if you want to show all the data.', 'ValueError')
                self._valGraph.value = str(self._dt.MAXVAL_TOSHOW)
        except Exception:
            self.warning('Only integer grater than zero or -1 if you want to show all the data.', 'ValueError')

    def read_message_queue(self, update_gui = False):
        """ Update data and the graph """

        if self.variaiables is None: # if the vector choice was not set
            self.variaiables = self.session.data.query("TYPE=='VAL' and MSG=='VECTOR_CHOICE'") # Find it
            for a, b in self.variaiables.iterrows():
                #print(b)
                print(b['+INFO'])
                self._dt.add(b['+INFO']) # Pass it to the myData

        data = self.session.data[self.COUNT:] # take care only the data unread till now
        self.COUNT += len(data) # update the counter
        
        res = data.query("TYPE=='{0}' and MSG=='Reward'".format(Session.MSGTYPE_STATE)) # Find the state Reward
        #print(self._dt._performance_L)
        #print(self._dt._performance_R)
        #print(self._dt._performance_L_mean)
        #print(self._dt._performance_R_mean)
        #print(COUNT - self.MAXVAL_TOSHOW)
        if update_gui: # it is True only one time: take a look to show() method
            self._progress.show()
            self._progress.value = 0
            self._progress.max = len(res)
        try:
            for a, b in res.iterrows(): # Parsing of the Reward states
                #print(b)
                if str(b['BPOD-INITIAL-TIME']) == 'nan':
                    self._dt.answare(-1) # wrong answare    
                else:
                    self._dt.answare(1) # right answare  

                # it's True one time so if the session is not running this loop
                # has all the data and this flag is True till the end of the loop 
                if update_gui: 
                    self._progress += 1

                QEventLoop() # provides a means of entering and leaving an event loop
            self._graph.draw()

        except Exception as err: # Catch all the exceptions
            if hasattr(self, '_timer'):
                self._timer.stop()
            logger.error(str(err), exc_info=True)
            QMessageBox.critical(self, "Error",
                                 "Unexpected error while loading session history. Pleas see log for more details.")

        if update_gui: # at the end Hide the bar
            self._progress.hide()

    def update(self):
        """ This methos is called many times till the session is stop """
        if not self.session.is_running: self._timer.stop()
        if len(self.session.data) > self.COUNT:
            self.read_message_queue()

    def __on_draw(self, figure):
        """ Redraws the figure """
        try:
            # Clean the figure, and create 2 subplot
            figure.clear()
            axes1 = figure.add_subplot(211)
            axes2 = figure.add_subplot(212)
            figure.tight_layout()

            # Figure 1
            #x = range(1, len(self._dt.data)+1)
            axes1.set_ylabel('Ports')
            axes1.set_xlabel('Trials')
            x, y = self._dt.ask_data_subset()
            #print(self._dt.data)
            axes1.scatter(x, y, s = 10, c = self._dt.data_answare)
            axes1.set_yticks([-1,1])
            axes1.set_yticklabels(['R','L'])
            axes1.set_ylim([-1.1,1.1])
            axes1.grid(which='major', linestyle = 'dashed', alpha = 0.4)
            #axes1.plot([1,2,3,4,5], [0,0,0,0,0], alpha = 0)

            # Figure 2
            x = range(1, len(self._dt.performance)+1)
            axes2.set_ylabel('Performance [%]')
            axes2.set_xlabel('Trials')
            axes2.scatter(x, self._dt.performance, facecolors='none', edgecolors='black', s =15)
            axes2.scatter([elem[0]+1 for elem in self._dt._performance_L_mean], [elem[1] for elem in self._dt._performance_L_mean], marker = 'x', c = 'red')
            axes2.scatter([elem[0]+1 for elem in self._dt._performance_R_mean], [elem[1] for elem in self._dt._performance_R_mean], marker = 'x', c = 'blue')
            #print(x)
            #print(self._dt.performance)
            axes2.plot([1,2,3,4,5], [0,0,0,0,0], alpha = 0)
            axes2.set_ylim([0,1])
            axes2.set_yticks(list(np.arange(0,1.1, 0.1)))
            axes2.set_yticklabels(list(map(str, range(0,110, 10))))
            axes2.grid(linestyle = 'dashed', alpha = 0.4)
        except Exception as err:
            print(err)

    def show(self, detached=False):
        """ Method used to show the windows """
        # Prevent the call to be recursive because of the mdi_area
        if not detached:
            if hasattr(self, '_show_called'):
                BaseWidget.show(self)
                return
            self._show_called = True
            self.mainwindow.mdi_area += self
            del self._show_called
        else:
            BaseWidget.show(self)
        
        self._stop = False  # flag used to close the gui in the middle of a loading
        self.read_message_queue(True) # Read all the data and update the plot
        
        if not self._stop and self.session.is_running: # if it is in real time
            self._timer.start(conf.SESSIONTREND_PLUGIN_REFRESH_RATE)

    def before_close_event(self):
        """ Method called before to close the windows """
        self._timer.stop()
        self._stop = True
        self.session.sessiontrend_action.setEnabled(True)
        self.session.sessiontrend_detached_action.setEnabled(True)

    @property
    def mainwindow(self):
        return self.session.mainwindow

    @property
    def title(self):
        return BaseWidget.title.fget(self)

    @title.setter
    def title(self, value):
        BaseWidget.title.fset(self, 'Session History: {0}'.format(value))
